#!/usr/bin/python
# -*- coding: utf-8 -*-

# Example Credit Mutuel TPE report mail to gDTC conversion.

import sys, io, os
import argparse, logging, email.parser, smtplib, ConfigParser
from email.mime.multipart import MIMEMultipart
from email.header import decode_header
from datetime import *

def date_diff ( d1, d2 ):
    if ( isinstance ( d1, date ) ):
        d1 = d1.strftime ( '%Y-%m-%d' )
    if ( isinstance ( d2, date ) ):
        d2 = d2.strftime ( '%Y-%m-%d' )
    d1_f = d1.split('-')
    d2_f = d2.split('-')
    diff = 0
    diff = int(d2_f[0])*12 - int(d1_f[0])*12 + int(d2_f[1]) - int(d1_f[1])
    if int(d1_f[2]) > 5 and int(d2_f[2]) < 5 and diff:
        diff -= 1
    if int(d2_f[2]) == 5:
        diff += 1
    return diff


######################################################################
### Usage
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--config', help='Alternative config file name')
    parser.add_argument('-v', dest='verbose', action='store_true', help='Be verbose')
    parser.add_argument('file', nargs='?', help='Email to parse')
    args = parser.parse_args()


######################################################################
### Config
config = ConfigParser.RawConfigParser()
config_filename = args.config
if not config_filename:
    config_filename = os.environ.get('PYDTC_CONFIG')
if not config_filename:
    config_filename = '/etc/pydtc.cfg'
if not config.read(config_filename):
    raise Exception('Unable to read config file pydtc.cfg')


######################################################################
### Setup logging
logger = logging.getLogger('parse-type.py')
format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
try:
    logfile = config.get('General','LogFile')
    logging.basicConfig(filename=logfile,level=logging.DEBUG,format=format)
    if args.verbose:
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter(format)
        ch.setFormatter(formatter)
        logger.addHandler(ch)
except:
    logging.basicConfig(level=logging.DEBUG,format=format)


######################################################################
### Open and parse email
try:
    if not args.file:
        csv = sys.stdin
    else:
        csv = io.open ( args.file )
except Exception as e:
    logger.error(e)
    sys.exit ( 1 )

mail = email.parser.Parser()
parsed = mail.parse(csv)

try:
    subject = ''
    for item in decode_header(parsed['subject']):
        subject = subject + ' ' + item[0].decode(item[1] or 'utf-8').encode('utf-8')
    if not subject.find('commandes') >= 0:
        logger.warning("Nothing to do: %s" % subject)
        sys.exit(0)
except Exception as e:
    logger.warning("Not an email: %s" % e)
    sys.exit(1)

mail_date = parsed.get ( 'Date' )
mail_date = mail_date[0:mail_date.rfind(' ')]
try:
    mail_date = datetime.strptime(mail_date, '%a, %d %b %Y %H:%M:%S')
except Exception as e:
    logger.warning(e)

######################################################################
### File CSV part
csv_filename = config.get('General', 'TPE') + '.csv'
for payload in parsed.walk():
    if payload.get_filename() == csv_filename:
        csv_str = payload.get_payload(None,True)

try:
    csv_str
except NameError:
    print 'Unable to find a CSV part'
    sys.exit ( 1 )

######################################################################
### Connect to DTC
if config.has_option('General', 'PythonPath'):
    sys.path.append ( config.get('General', 'PythonPath') )
from pyDTC import *


dtc=pyDTC('mysql://', config_filename, False)


######################################################################
### Prepare report
html = ''
txt = ''
num_errors = 0
num_payments = 0

######################################################################
### Iterate over CSV file
for line in csv_str.split("\n"):
    if line and len(line) > 1 and line [ 0 ] == '"':
        num_payments += 1

        fields = line.strip("\n").split(',')
        try:
            ref = fields [ 1 ] . strip ( '"' )
            status = fields [ 8 ] . strip ( '"' )
            amount = fields [ 9 ] . strip ( '"' )
        except Exception as e:
            logger.warning('Malformed line «', line, '»')

        # If different of recouvrement, then it is an error
        if status != 'recouvrement':
            num_errors += 1
            q = dtc.query ( Subscription ).\
                filter ( Subscription.voucher == ref ).\
                filter ( Subscription.payment_mode == 'cbweb_recur' ).\
                order_by ( asc(Subscription.prelevement_start_date) ).\
                limit ( 1 )
            # Is payment found in DTC ?
            if q.count ( ) == 1:
                e = q [ 0 ]
                if not dtc.query ( Non_Payment ).\
                   filter ( Non_Payment.voucher == e.voucher ).\
                   filter ( Non_Payment.date == mail_date ).\
                   count():
                    dtc.session.add ( Non_Payment ( e.subscription_id, ref, mail_date, e.amount ) )
                    dtc.session.flush()
                all_fails = dtc.query ( Non_Payment ).\
                          filter ( Non_Payment.voucher == e.voucher ).\
                          order_by ( desc(func.date(Non_Payment.date)) )
                num = all_fails.count()
                diff = 0
                for fail in all_fails:
                    if date_diff ( fail.date, all_fails[0].date ) - diff <= 1:
                        diff += 1

                if date_diff ( mail_date, e.prelevement_start_date ):
                    failcount = float(num) / date_diff ( e.prelevement_start_date, mail_date ) * 100
                else:
                    failcount = 1
                txt += " * %s %s: %.2f%% en échec depuis le %s (%d consecutif(s))\n"% \
                    ( e.membership.actor.person.firstname, e.membership.actor.person.name, \
                          failcount, e.prelevement_start_date.strftime('%x'), diff )
                txt += "   %s/?action=edit&table=%s&id=%d\n\n" % \
                    ( config.get('gDTC', 'Admin_URL'), e.membership.actor.actor_type, e.membership.actor.actor_id )
                html += ( "<li><a href=\"%s/?action=edit&table=%s&actor_id=%d\">%s %s</a> : %.2f%% en échec depuis le %s (%d consecutif(s))</li>\n"% \
                              ( config.get('gDTC', 'Admin_URL'), e.membership.actor.actor_type,\
                                    e.membership.actor.actor_id,\
                                    e.membership.actor.person.firstname, e.membership.actor.person.name, \
                                    failcount, e.prelevement_start_date.strftime('%x'), diff ) )

csv.close()

style = "ul{list-style:none} li{margin:0.5em;} li a{min-width: 200px;display:inline-block;}"
report_html = ( "<html><body><style>%s</style><p>%d paiements en erreur sur %d</p><ul>\n" % ( style, num_errors, num_payments ) ) + html + "</body></html>"
report_txt = ( "%d paiements en erreur sur %d\n\n" % ( num_errors, num_payments ) ) + txt


######################################################################
### Build mail
msg = MIMEMultipart('')
msg [ 'From' ] = config.get('Mail', 'Robot')
msg [ 'To' ] = config.get('Mail', 'Recipient')
msg [ 'Subject' ] = 'Rapport de remise TPE récurrent'

mime = mail.parsestr ( '' )
mime.set_payload ( report_txt )
mime.set_type ( 'text/txt' )
msg.attach ( mime )

mime = mail.parsestr ( '' )
mime.set_payload ( report_html )
mime.set_type ( 'text/html' )
msg.attach ( mime )

try:
    s = smtplib.SMTP ( config.get('Mail','Server') )
    s.sendmail ( config.get('Mail','Robot'), config.get('Mail','Recipient'), str(msg) )
    s.quit
    logger.info ( "Sent mail to %s" % ( config.get('Mail','Recipient') ) )
except Exception as e:
    logger.error("SMTP error: " + str(e))


######################################################################
### All is done, write log success to make Nagios happy
logger.info ( "Run on %s" % ( datetime.now().strftime('%Y-%m-%d %H:%M:%S') ) )

