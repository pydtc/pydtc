from distutils.core import setup

setup(
    name='pyDTC',
    version='0.1.0',
    author='Benjamin Drieu',
    author_email='bdrieu@april.org',
    packages=['pyDTC'],
    scripts=['bin/parse-tpe.py'],
    url='https://gitorious.org/pyDTC',
    license='LICENSE.txt',
    description='Accounting files parsing and handling classes.',
    long_description=open('README.txt').read(),
    install_requires=[
        "lxml"
    ],
)
