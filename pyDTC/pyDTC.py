#!/usr/bin/python

from sqlalchemy import *
from sqlalchemy.orm import *
from sqlalchemy.orm.interfaces import SessionExtension
from sqlalchemy.orm.attributes import *
from sqlalchemy.ext.declarative import *
from sqlalchemy import func

import sqlalchemy.pool as pool

import ConfigParser
import os

from datetime import *

import MySQLdb

def getconn():
    config = ConfigParser.RawConfigParser()
    config_filename = pyDTC.config_filename
#    config_filename = os.environ.get('PYDTC_CONFIG')
    if not config_filename:
        config_filename = '/etc/pydtc.cfg'
    if not config.read(config_filename):
        config_filename = 'pydtc.cfg'
    if not config.read(config_filename):
        raise Exception('Unable to read config file pydtc.cfg')
    
    c = MySQLdb.connect(user=config.get('General', 'User'), 
                        passwd=config.get('General', 'Password'), 
                        host=config.get('General', 'Host'), 
                        db=config.get('General', 'DB'))

    c.query ( "SET names 'utf8';" );
    return c


class DTCSessionExtension(SessionExtension):
    def after_flush(self,session,flush_context):
        '''
        Check all instances for owner_id==user.id
        '''

        for instance in session.new:
            try:
                if instance.__class__.__name__ != 'Modification':
                    classname = instance.__class__.__name__.lower()
                    print 'running policy for new %s (%s)' % ( classname, instance )
                    session.add ( Modification ( 'created', classname, classname + '_id',
                                                 instance . __dict__ [ classname + '_id' ],
                                                 False, False, False, 'benj', Date() ) )

            except Exception,ex:
                print ex
                raise



class AttrLogExt(AttributeExtension):
    def __init__(self, session):
        self.session = session
        AttributeExtension.__init__(self)

    def set ( self, state, value, oldvalue, initiator ):
        o = state . obj ( )
        classname = initiator . class_ . __name__ . lower ( )
        if classname != 'modification' and oldvalue != value and o . __dict__ . has_key ( classname + '_id' ) and o . __dict__ [ classname + '_id' ]:
            colname = initiator.parent_token.__str__()
            colname = colname[colname.find('.')+1:]
            print "Modif column %s of %s to %s" % (colname, o, value)
            print o . __dict__
            self.session.add ( Modification ( 'changed', classname, classname + '_id',
                                              o . __dict__ [ classname + '_id' ],
                                              colname, oldvalue, value, 'benj', Date() ) )

        return value


class pyDTC:
    def __init__(self, db, config_filename=False, print_debug=False):
        pyDTC.config_filename = config_filename
        self.engine = create_engine(db, echo=print_debug, creator=getconn)
        self.engine.dialect.convert_unicode = True
        Session = sessionmaker(autocommit=True)
        Session.configure(extension=DTCSessionExtension(), bind=self.engine)
        self.session = Session ( )

        metadata = MetaData()

        person = Table ( 'person', metadata,
                         Column ( 'actor_id', Integer, ForeignKey('actor.actor_id'), primary_key=True ),
                         Column ( 'firstname', String ),
                         Column ( 'name', String ),
                         Column ( 'email', String ) )
        mapper(Person, person, properties={
            'actor_id' : column_property(person.c.actor_id, extension=AttrLogExt(self.session)),
            'firstname' : column_property(person.c.firstname, extension=AttrLogExt(self.session)),
            'name' : column_property(person.c.name, extension=AttrLogExt(self.session)),
            'email' : column_property(person.c.email, extension=AttrLogExt(self.session))
        })

        entity = Table ( 'entity', metadata,
                         Column ( 'actor_id', Integer, ForeignKey('actor.actor_id'), primary_key=True ),
                         Column ( 'name', String ),
                         Column ( 'email', String ) )
        mapper(Entity, entity, properties={
            'actor_id' : column_property(entity.c.actor_id, extension=AttrLogExt(self.session)),
            'name' : column_property(entity.c.name, extension=AttrLogExt(self.session)),
            'email' : column_property(entity.c.email, extension=AttrLogExt(self.session))
        })

        actor = Table ( 'actor', metadata,
                        Column ( 'actor_id', Integer, primary_key=True ),
                        Column ( 'nickname', String ),
                        Column ( 'actor_type', String ),
                        Column ( 'actor_status', Integer ),
                        Column ( 'password', String ),
                        Column ( 'cle', String ),
                        Column ( 'is_admin', Boolean ) )
        mapper(Actor, actor, properties={
            'person' : relationship(Person, order_by=person.c.actor_id, backref='actor', single_parent=True, uselist=False),
            'entity' : relationship(Entity, order_by=entity.c.actor_id, backref='actor', single_parent=True, uselist=False),
            'actor_id' : column_property(actor.c.actor_id, extension=AttrLogExt(self.session)),
            'nickname' : column_property(actor.c.nickname, extension=AttrLogExt(self.session)),
            'actor_type' : column_property(actor.c.actor_type, extension=AttrLogExt(self.session)),
            'actor_status' : column_property(actor.c.actor_status, extension=AttrLogExt(self.session)),
            'password' : column_property(actor.c.password, extension=AttrLogExt(self.session)),
            'cle' : column_property(actor.c.cle, extension=AttrLogExt(self.session)),
            'is_admin' : column_property(actor.c.is_admin, extension=AttrLogExt(self.session))            
        })

        membership = Table('membership', metadata,
                           Column( 'membership_id', Integer, primary_key=True),
                           Column( 'actor_id', Integer, ForeignKey('actor.actor_id')),
                           Column( 'start_date', Date),
                           Column( 'end_date', Date),
                           Column( 'renewal_date', Date),
                           Column( 'validated', Boolean),
                           Column( 'admin_info', String) )
        mapper(Membership, membership, properties={
            'actor' : relationship(Actor, order_by=membership.c.membership_id, backref='memberships'),

            'membership_id' : column_property(membership.c.membership_id, extension=AttrLogExt(self.session)),
            'actor_id' : column_property(membership.c.actor_id, extension=AttrLogExt(self.session)),
            'start_date' : column_property(membership.c.start_date, extension=AttrLogExt(self.session)),
            'end_date' : column_property(membership.c.end_date, extension=AttrLogExt(self.session)),
            'renewal_date' : column_property(membership.c.renewal_date, extension=AttrLogExt(self.session)),
            'validated' : column_property(membership.c.validated, extension=AttrLogExt(self.session)),
            'admin_info' : column_property(membership.c.admin_info, extension=AttrLogExt(self.session)),
        })
        
        subscription = Table('subscription', metadata,
                             Column ( 'subscription_id', Integer, primary_key=True ),
                             Column ( 'membership_id', Integer, ForeignKey('membership.membership_id' ) ),
                             Column ( 'payment_mode', String ),
                             Column ( 'amount', Float ),
                             Column ( 'reference', String ),
                             Column ( 'voucher', String ),
                             Column ( 'start_date', Date ),
                             Column ( 'end_date', Date ),
                             Column ( 'payment_received_date', Date ),
                             Column ( 'invoice_sent_date', Date ),
                             Column ( 'prelevement_start_date', Date ),
                             Column ( 'prelevement_end_date', Date ),
                             Column ( 'prelevement_amount', Integer ),
                             Column ( 'prelevement_periodicity', Integer ),
                             Column ( 'cbrecur_initial_amount', Float ),
                             Column ( 'cbrecur_expiration_date', Date ),
                             Column ( 'cbrecur_acked_date', Date ),
                             Column ( 'cbrecur_ack_result', String ),
                             Column ( 'cbrecur_prorata', Integer ),
                             Column ( 'admin_info' , String ),
                             Column ( 'commission_percentage', Float ),
                             Column ( 'commission_minimum', Float ),
                             Column ( 'commission_fixed', Float ) )
        mapper(Subscription, subscription, properties={
            'membership' : relationship(Membership, order_by=subscription.c.subscription_id, backref='subscriptions'),

            'subscription_id' : column_property(subscription.c.subscription_id, extension=AttrLogExt(self.session)),
            'membership_id' : column_property(subscription.c.membership_id, extension=AttrLogExt(self.session)),
            'payment_mode' : column_property(subscription.c.payment_mode, extension=AttrLogExt(self.session)),
            'amount' : column_property(subscription.c.amount, extension=AttrLogExt(self.session)),
            'reference' : column_property(subscription.c.reference, extension=AttrLogExt(self.session)),
            'voucher' : column_property(subscription.c.voucher, extension=AttrLogExt(self.session)),
            'start_date' : column_property(subscription.c.start_date, extension=AttrLogExt(self.session)),
            'end_date' : column_property(subscription.c.end_date, extension=AttrLogExt(self.session)),
            'payment_received_date' : column_property(subscription.c.payment_received_date, extension=AttrLogExt(self.session)),
            'invoice_sent_date' : column_property(subscription.c.invoice_sent_date, extension=AttrLogExt(self.session)),
            'prelevement_start_date' : column_property(subscription.c.prelevement_start_date, extension=AttrLogExt(self.session)),
            'prelevement_end_date' : column_property(subscription.c.prelevement_end_date, extension=AttrLogExt(self.session)),
            'prelevement_amount' : column_property(subscription.c.prelevement_amount, extension=AttrLogExt(self.session)),
            'prelevement_periodicity' : column_property(subscription.c.prelevement_periodicity, extension=AttrLogExt(self.session)),
            'admin_info' : column_property(subscription.c.admin_info, extension=AttrLogExt(self.session)),
            'commission_percentage' : column_property(subscription.c.commission_percentage, extension=AttrLogExt(self.session)),
            'commission_minimum' : column_property(subscription.c.commission_minimum, extension=AttrLogExt(self.session)),
            'commission_fixed' : column_property(subscription.c.commission_fixed, extension=AttrLogExt(self.session)),
        })

        non_payment = Table('non_payment', metadata,
                            Column( 'non_payment_id', Integer, primary_key=True),
                            Column( 'subscription_id', Integer, ForeignKey('subscription.subscription_id')),
                            Column( 'date', Date),
                            Column ( 'voucher', String ),
                            Column ( 'amount', Float ) )
        mapper(Non_Payment, non_payment, properties={
            'subscription' : relationship(Subscription, order_by=subscription.c.subscription_id, backref='non_payments'),
            'date' : column_property(non_payment.c.date, extension=AttrLogExt(self.session)),
            'voucher' : column_property(non_payment.c.voucher, extension=AttrLogExt(self.session)),
            'amount' : column_property(non_payment.c.amount, extension=AttrLogExt(self.session)),
        })


    def query(self, q, *rest):
        if rest:
            return self.session.query(q, rest)
        else:
            return self.session.query(q)



##############################################################################
# Actor
                
class Actor(object):
    __tablename__ = 'actor'

    def __init__(self, nickname, actor_status, actor_type, password):
        self.nickname = nickname
        self.actor_status = actor_status
        self.actor_type = actor_type
        self.password = password

    def details(self):
        if self.actor_type == 'person':
            return self.person
        else:
            return self.entity

    def __repr__(self):
       return "<Actor('%s','%s', '%s')>" % (self.nickname, self.actor_status, self.password)



##############################################################################
# Person
               
class Person(object):
    __tablename__ = 'person'

    def __init__(self, actor_id, firstname, name):
        self.actor_id = actor_id
        self.firstname = firstname
        self.name = name

    def __repr__(self):
       return "<Person('%d',%s %s)>" % (self.actor_id, self.firstname, self.name)

    def fullname(self):
        return "%s %s" % ( self.firstname, self.name )



##############################################################################
# Entity

class Entity(object):
    __tablename__ = 'entity'

    def __init__(self, actor_id, name):
        self.actor_id = actor_id
        self.name = name

    def __repr__(self):
       return "<Entity('%d',%s)>" % (self.actor_id, self.name)

    def fullname(self):
        return self.name



##############################################################################
# Membership

class Membership(object):
    __tablename__ = 'membership'

    def __init__(self, actor_id, start_date, renewal_date, end_date, validated = 0):
        self.actor_id = actor_id
        self.start_date = start_date
        self.renewal_date = start_date
        self.end_date = end_date
        self.validated = validated

    def __repr__(self):
       return "<Membership('%d','%d', %s -> %s)>" % (self.membership_id, self.actor_id, self.start_date, self.end_date)



##############################################################################
# Subscription

class Subscription(object):
    __tablename__ = 'subscription'

    def __init__(self, actor_id, start_date, renewal_date, end_date, validated = 0):
        self.actor_id = actor_id
        self.start_date = start_date
        self.renewal_date = start_date
        self.end_date = end_date
        self.validated = validated

    def __repr__(self):
        return "<Subscription('%d','%d', %d euros (%s) %s -> %s)>" % \
               (self.subscription_id, self.membership_id, self.amount, self.payment_mode, self.start_date, self.end_date)

    def is_prelevement(self):
        return self.payment_mode == 'prelevement'

    def increment_prelevement_date ( self, d ):
        month = d.month + self.prelevement_periodicity
        year = d.year
        if month > 12:
            month = month % 12
            if not month: month = 12
            year += 1
        return date ( year, month, d.day )



##############################################################################
# Non Payment

class Non_Payment(object):
    __tablename__ = 'non_payment'

    def __init__(self, subscription_id, voucher, date, amount):
        self.subscription_id = subscription_id
        self.voucher = voucher
        self.date = date
        self.amount = amount

    def __repr__(self):
        return "<Non_Payment('%d','%d', %d euros (%s), %s>" % \
               (self.non_payment_id, self.subscription_id, self.amount, self.voucher, self.date)


##############################################################################
# Modification

Base = declarative_base()
class Modification(Base):
    __tablename__ = 'modification'
    id_modification = Column(Integer, primary_key=True)
    type_modification = Column(String)
    table_name = Column(String)
    id_table = Column(String)
    id_field = Column(Integer)
    field_name = Column(String)
    old_value = Column(Text)
    new_value = Column(Text)
    date_modification = Column(Time, default=datetime.now())
    user = Column(Text)

    def __init__( self, type_modification, table_name, id_table, id_field, \
                  field_name, old_value, new_value, user, date_modification = False ):
        self.type_modification = type_modification
        self.table_name = table_name
        self.id_table = id_table
        self.id_field = id_field
        self.field_name = field_name
        self.old_value = old_value
        self.new_value = new_value
        if self.date_modification:
            self.date_modification = date_modification
        self.user = user

    def __repr__(self):
        return "<Modification('%s','%s', %s -> %s)>" % (self.type_modification, self.table_name, self.old_value, self.new_value)

